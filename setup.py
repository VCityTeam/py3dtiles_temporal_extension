import os
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))

requirements = (
    'py3dtiles'
)

dev_requirements = (
    'flake8',
    'pytest',
    'pytest-cov',
    'pytest-benchmark',
    'line_profiler'
)

doc_requirements = (
    'sphinx',
    'sphinx_rtd_theme',
    'sphinx-multiversion',
)

setup(
    name='py3dtiles_temporal_extension',
    version='1.0',
    description="Temporal extension of Py3DTiles",
    long_description="",
    url='https://gitlab.com/VCityTeam/py3dtiles_temporal_extension',
    author='VCityTeam',
    author_email='',
    license='Apache License Version 2.0',
    python_requires='>=3.7',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3.9',
    ],
    packages=find_packages(),
    install_requires=requirements,
    test_suite="tests",
    extras_require={
        'dev': dev_requirements,
        'doc': doc_requirements
    },
    zip_safe=False  # zip packaging conflicts with Numba cache (#25)
)
