# -*- coding: utf-8 -*-
from .temporal_extension_transaction import TemporalTransaction


class TemporalTransactionAggregate(TemporalTransaction):
    """
    An aggregate of Temporal Primary Transactions (since the base class is
    abstract).
    """

    def __init__(self):
        super().__init__()
        self.define_attributes()

    def define_attributes(self):
        # Refer to TemporalTransaction::replicate_from()
        self.transactions = list()

    def set_transactions(self, transactions):
        if not isinstance(transactions, list):
            raise AttributeError('Setting transactions requires a list argument.')
        self.transactions = transactions

    def append_transaction(self, transaction):
        self.transactions.append(transaction)

    def to_dict(self) -> dict:
        dict_data = super().to_dict()

        dict_data['transactions'] = [t.to_dict() for t in self.transactions]

        return dict_data
