from py3dtiles.tileset.extension import BaseExtension


class TemporalExtension(BaseExtension):

    def __init__(self, name: str, owner=None):
        super().__init__(name)
        self.owner = owner

    def set_owner(self, owner):
        self.owner = owner

    def get_owner(self):
        return self.owner

    @classmethod
    def from_dict(self, extension_dict):
        return TemporalExtension('temporal_extension')

    def to_dict(self):
        return {}
