# -*- coding: utf-8 -*-
from .temporal_extension import TemporalExtension


class TemporalTransaction(TemporalExtension):
    """
    Temporal Transaction is an element of the Temporal TileSet extension.
    """
    # Total number of created transactions (as opposed to existing transactions
    # the difference being that the counter doesn't take into account the
    # deleted transactions)
    transactions_counter = 0

    def __init__(self):
        super().__init__('3DTILES_temporal')

        # The identifier is defaulted with a value handled by the class.
        # Yet the identifier is only handled partially by the class since
        # its value can be overwritten (and without warning in doing so)
        self.id = str(TemporalTransaction.transactions_counter)
        TemporalTransaction.transactions_counter += 1
        self.startDate = None
        self.endDate = None
        self.tags = list()
        self.source = list()
        self.destination = list()

    def define_attributes(self):
        print('This method should have been overloaded in derived class !')
        pass

    def replicate_from(self, to_be_replicated: 'TemporalTransaction'):
        """
        Overwrite the attributes of this object with the ones of the given
        (argument) object.
        :param to_be_replicated: the object attributes that must be replicated
                                 to the ones of self.
        """
        to_be_replicated.replicate_attributes_to(self)
        # Because the attributes _dictionary_ was overwritten we have to
        # redefine the derived class(es) attributes
        self.define_attributes()

    def replicate_attributes_to(self, target: 'TemporalTransaction'):
        """
        Overwrite the attributes of the target with the ones of this object.
        :param target: the targeted object.
        """
        target.startDate = self.startDate
        target.endDate = self.endDate
        target.tags = self.tags
        target.source = self.source
        target.destination = self.destination

    def set_id(self, identifier):
        self.id = identifier

    def set_start_date(self, date):
        self.startDate = date

    def set_end_date(self, date):
        self.endDate = date

    def set_tags(self, tags):
        if not isinstance(tags, list):
            raise AttributeError('Setting tags requires a list argument.')
        self.tags = tags

    def append_tag(self, tag):
        self.tags.append(tag)

    def set_sources(self, features):
        if not isinstance(features, list):
            raise AttributeError('Setting old features requires a list argument.')
        self.source = features

    def append_source(self, feature):
        self.source.append(feature)

    def set_destinations(self, features):
        if not isinstance(features, list):
            raise AttributeError('Setting new features requires a list argument.')
        self.destination = features

    def append_destination(self, feature):
        self.destination.append(feature)

    def to_dict(self) -> dict:
        dict_data = {}

        dict_data['id'] = self.id
        dict_data['startDate'] = self.startDate
        dict_data['endDate'] = self.endDate
        dict_data['source'] = [s for s in self.source]
        dict_data['destination'] = [d for d in self.destination]
        dict_data['tags'] = [t for t in self.tags]

        return dict_data
