# -*- coding: utf-8 -*-
from .temporal_extension_transaction import TemporalTransaction


class TemporalPrimaryTransaction(TemporalTransaction):
    """
    Temporal Primary Transaction represents the atomic Transaction.
    """

    def __init__(self):
        super().__init__()
        self.define_attributes()

    def define_attributes(self):
        # Refer to TemporalTransaction::replicate_from()
        self.type = None

    def set_type(self, new_type):
        if new_type not in \
                ["creation", "demolition", "modification", "union", "division"]:
            raise AttributeError('Unknown type of transaction.')
        self.type = new_type

    def to_dict(self) -> dict:
        dict_data = super().to_dict()

        dict_data['type'] = self.type

        return dict_data
