# -*- coding: utf-8 -*-
from .temporal_extension import TemporalExtension


class TemporalVersionTransition(TemporalExtension):
    """
    Temporal Version Transition is an element of the Temporal TileSet extension.
    """

    def __init__(self):
        super().__init__('3DTILES_temporal')

        self.t_name = None
        self.startDate = None
        self.endDate = None
        self._from = None
        self.to = None
        self.reason = None
        self.type = None
        self.transactions = list()

    def set_name(self, name):
        self.t_name = name

    def set_start_date(self, date):
        self.startDate = date

    def set_end_date(self, date):
        self.endDate = date

    def set_from(self, from_arg):
        self._from = from_arg

    def set_to(self, to):
        self.to = to

    def set_reason(self, reason):
        self.reason = reason

    def set_type(self, type):
        self.type = type

    def set_transactions(self, transactions):
        if not isinstance(transactions, list):
            raise AttributeError('Setting transactions requires a list argument.')
        self.transactions = transactions

    def append_transaction(self, transaction):
        self.transactions.append(transaction)

    def to_dict(self) -> dict:
        dict_data = {}

        dict_data['name'] = self.t_name
        dict_data['startDate'] = self.startDate
        dict_data['endDate'] = self.endDate
        dict_data['from'] = self._from
        dict_data['to'] = self.to
        dict_data['reason'] = self.reason
        dict_data['type'] = self.type
        dict_data['transactions'] = [t for t in self.transactions]

        return dict_data
