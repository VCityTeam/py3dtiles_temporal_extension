# -*- coding: utf-8 -*-
from .temporal_extension import TemporalExtension
from .temporal_extension_utils import temporal_extract_bounding_dates


class TemporalBoundingVolume(TemporalExtension):
    """
    Temporal Bounding Volume is an TemporalExtension of a Bounding Volume.
    """

    def __init__(self, owner=None):
        super().__init__('3DTILES_temporal', owner)

        self.startDate = None
        self.endDate = None

    def set_start_date(self, date):
        self.startDate = date

    def get_start_date(self):
        return self.startDate

    def set_end_date(self, date):
        self.endDate = date

    def get_end_date(self):
        return self.endDate

    def sync_dates(self):
        bounding_dates = temporal_extract_bounding_dates(
            TemporalBoundingVolume.get_children(self.get_owner()))
        self.set_start_date(bounding_dates['start_date'])
        self.set_end_date(bounding_dates['end_date'])

    @classmethod
    def get_children(cls, owner):
        children_tbv = list()
        children_bv = [child.bounding_volume for child in owner.get_all_children()]
        for bounding_volume in children_bv:
            temporal_bv = bounding_volume.extensions['3DTILES_temporal']
            if not temporal_bv:
                raise AttributeError('This bounding volume lacks its temporal extension.')
            children_tbv.append(temporal_bv)
        return children_tbv

    def to_dict(self) -> dict:
        dict_data = {}

        if self.get_owner() is not None:
            self.sync_dates()

        dict_data['startDate'] = self.startDate
        dict_data['endDate'] = self.endDate

        return dict_data
