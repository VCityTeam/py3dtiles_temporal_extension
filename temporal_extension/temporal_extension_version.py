# -*- coding: utf-8 -*-
from .temporal_extension import TemporalExtension


class TemporalVersion(TemporalExtension):
    """
    Temporal Version is an element of the Temporal TileSet extension.
    """

    def __init__(self):
        super().__init__('3DTILES_temporal')

        self.id = None
        self.startDate = None
        self.endDate = None
        self.name = None
        self.versionMembers = list()
        self.tags = list()

    def set_id(self, id):
        self.id = id

    def set_start_date(self, date):
        self.startDate = date

    def set_end_date(self, date):
        self.endDate = date

    def set_name(self, name):
        self.name = name

    def set_version_members(self, versions):
        if not isinstance(versions, list):
            raise AttributeError('Setting version members requires a list argument.')
        self.versionMembers = versions

    def append_version_member(self, version):
        self.versionMembers.append(version)

    def set_tags(self, tags):
        if not isinstance(tags, list):
            raise AttributeError('Setting tags requires a list argument.')
        self.tags = tags

    def append_tag(self, tag):
        self.tags.append(tag)

    def to_dict(self) -> dict:
        dict_data = {}

        dict_data['name'] = self.name
        dict_data['startDate'] = self.startDate
        dict_data['endDate'] = self.endDate
        dict_data['id'] = self.id
        dict_data['versionMembers'] = [vm for vm in self.versionMembers]
        dict_data['tags'] = [t for t in self.tags]

        return dict_data
