# -*- coding: utf-8 -*-
from .temporal_extension import TemporalExtension


class TemporalBatchTable(TemporalExtension):
    """
    Temporal Batch Table is an TemporalExtension of a Batch Table.
    """

    def __init__(self):
        super().__init__('3DTILES_temporal')

        self.startDates = list()
        self.endDates = list()
        self.featureIds = list()

    def set_start_dates(self, dates):
        if not isinstance(dates, list):
            raise AttributeError('Setting startDates requires a list argument.')
        self.startDates = dates

    def append_start_date(self, date):
        self.startDates.append(date)

    def set_end_dates(self, dates):
        if not isinstance(dates, list):
            raise AttributeError('Setting endDates requires a list argument.')
        self.endDates = dates

    def append_end_date(self, date):
        self.endDates.append(date)

    def set_feature_ids(self, ids):
        if not isinstance(ids, list):
            raise AttributeError('Setting feature IDs requires a list argument.')
        self.featureIds = ids

    def append_feature_id(self, id):
        self.featureIds.append(id)

    def to_dict(self) -> dict:
        dict_data = {}

        dict_data['startDates'] = [sd for sd in self.startDates]
        dict_data['endDates'] = [ed for ed in self.endDates]
        dict_data['featureIds'] = [id for id in self.featureIds]

        return dict_data
