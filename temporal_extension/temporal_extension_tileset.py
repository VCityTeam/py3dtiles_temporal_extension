# -*- coding: utf-8 -*-
from .temporal_extension import TemporalExtension
from .temporal_extension_transaction import TemporalTransaction


class TemporalTileSet(TemporalExtension):
    """
    Temporal Tile Set is an TemporalExtension of a Tile Set.
    """

    def __init__(self, owner=None):
        super().__init__('3DTILES_temporal', owner)

        self.startDate = None
        self.endDate = None
        self.transactions = list()
        self.versions = list()
        self.versionTransitions = list()

    def set_start_date(self, date):
        self.startDate = date

    def get_start_date(self):
        return self.startDate

    def set_end_date(self, date):
        self.endDate = date

    def get_end_date(self):
        return self.endDate

    def set_transactions(self, transactions):
        if not isinstance(transactions, list):
            raise AttributeError('Setting transactions requires a list argument.')
        self.transactions = transactions

    def append_transaction(self, transaction):
        if not isinstance(transaction, TemporalTransaction):
            raise AttributeError('Append_transaction requires a transaction argument.')
        self.transactions.append(transaction)

    def set_versions(self, versions):
        if not isinstance(versions, list):
            raise AttributeError('Setting versions requires a list argument.')
        self.versions = versions

    def append_version(self, version):
        self.versions.append(version)

    def set_version_transitions(self, version_transitions):
        if not isinstance(version_transitions, list):
            raise AttributeError('Setting version transitions requires a list argument.')
        self.versionTransitions = version_transitions

    def append_version_transition(self, version_transition):
        self.versionTransitions.append(version_transition)

    def to_dict(self) -> dict:
        dict_data = {}

        if self.get_owner() is not None:
            root_tile_bv = self.get_owner().root_tile.bounding_volume
            # Retrieve the temporal bounding volume
            root_tile_tbv = root_tile_bv.extensions['3DTILES_temporal']
            self.set_start_date(root_tile_tbv.get_start_date())
            self.set_end_date(root_tile_tbv.get_end_date())

        dict_data['startDate'] = self.startDate
        dict_data['endDate'] = self.endDate
        dict_data['transactions'] = [t.to_dict() for t in self.transactions]
        dict_data['versions'] = [v.to_dict() for v in self.versions]
        dict_data['versionTransitions'] = [vt.to_dict() for vt in self.versionTransitions]

        return dict_data
