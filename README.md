# Py3DTiles Temporal Extension

Implements the [Temporal Extension](https://github.com/VCityTeam/UD-SV/tree/master/3DTilesTemporalExtention) in [Py3DTiles](https://gitlab.com/Oslandia/py3dtiles)
