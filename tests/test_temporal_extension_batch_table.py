# -*- coding: utf-8 -*-

import json
import unittest
from temporal_extension import TemporalBatchTable
from helper_test import HelperTest


class Test_TemporalBatchTable(unittest.TestCase):
    """
    Batch Table extension of the Temporal applicative extension
    """

    def build_sample(self):
        """
        Programmatically define the reference a sample.
        :return: the sample as TemporalBatchTable object.
        """
        tbt = TemporalBatchTable()

        tbt.set_start_dates(["2018-01-01", "2028-02-02"])
        tbt.append_start_date("2038-03-03")
        tbt.set_end_dates(["2019-01-01", "2029-02-02"])
        tbt.append_end_date("2039-03-03")
        tbt.set_feature_ids(["1", "2"])
        tbt.append_feature_id("3")

        return tbt

    def test_to_dict(self):
        return self.build_sample().to_dict()

    def test_build_sample_and_compare_reference_file(self):
        """
        Build the sample, load the version from the reference file and
        compare them (in memory as opposed to "diffing" files)
        """
        json_tbt = self.build_sample().to_dict()
        json_tbt_reference = HelperTest().load_json_reference_file(
            'temporal_extension_batch_table_sample.json')
        if not json_tbt.items() == json_tbt_reference.items():
            self.fail()


if __name__ == "__main__":
    unittest.main()
