# -*- coding: utf-8 -*-

import json
import unittest
from temporal_extension import TemporalVersionTransition
from helper_test import HelperTest


class Test_TemporalVersionTransition(unittest.TestCase):
    """
    Batch Table extension of the Temporal applicative extension
    """

    @classmethod
    def build_sample(cls):
        """
        Programmatically define the reference a sample.
        :return: the sample as TemporalBatchTable object.
        """
        tvt = TemporalVersionTransition()

        tvt.set_name("Version Transition Name")
        tvt.set_start_date("2018-01-01")
        tvt.set_end_date("2019-01-01")
        tvt.set_from(100)
        tvt.set_to(200)
        tvt.set_reason("Reason of evolution between two versions")
        tvt.set_type("merge")
        tvt.set_transactions([1000])
        tvt.append_transaction(2000)

        return tvt

    def test_to_dict(self):
        return self.build_sample().to_dict()

    def test_build_sample_and_compare_reference_file(self):
        """
        Build the sample, load the version from the reference file and
        compare them (in memory as opposed to "diffing" files)
        """
        json_tvt = self.build_sample().to_dict()
        json_tvt_reference = HelperTest().load_json_reference_file(
            'temporal_extension_version_transition_sample.json')
        if not json_tvt.items() == json_tvt_reference.items():
            self.fail()


if __name__ == "__main__":
    unittest.main()
