# -*- coding: utf-8 -*-

import json
import unittest
from temporal_extension import TemporalTransaction
from helper_test import HelperTest


class Test_TemporalTransaction(unittest.TestCase):
    """
    Batch Table extension of the Temporal applicative extension
    """

    @classmethod
    def build_sample(cls):
        """
        Programmatically define the reference a sample.
        :return: the sample as TemporalBatchTable object.
        """
        tt = TemporalTransaction()

        tt.set_start_date("2018-01-01")
        tt.set_end_date("2019-01-01")
        tt.set_tags(["heightened"])
        tt.set_sources(["some-id"])
        tt.append_source("some-other-id")
        tt.set_destinations(["a given id"])
        tt.append_destination("another given id")

        return tt

    def test_to_dict(self):
        return self.build_sample().to_dict()

    def test_build_sample_and_compare_reference_file(self):
        """
        Build the sample, load the version from the reference file and
        compare them (in memory as opposed to "diffing" files)
        """
        json_tt = self.build_sample().to_dict()
        json_tt_reference = HelperTest().load_json_reference_file(
            'temporal_extension_transaction_sample.json')
        # We do not want to compare the identifiers (that must differ):
        del json_tt['id']
        del json_tt_reference['id']
        if not json_tt.items() == json_tt_reference.items():
            self.fail()


if __name__ == "__main__":
    unittest.main()
