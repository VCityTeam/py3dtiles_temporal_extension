# -*- coding: utf-8 -*-

import json
import unittest
from temporal_extension import TemporalBoundingVolume
from helper_test import HelperTest


class Test_TemporalBoundingVolume(unittest.TestCase):
    """
    Batch Table extension of the Temporal applicative extension
    """

    def build_sample(self):
        """
        Programmatically define the reference a sample.
        :return: the sample as TemporalBatchTable object.
        """
        tbv = TemporalBoundingVolume()

        tbv.set_start_date("2018-01-01")
        tbv.set_end_date("2019-01-01")

        return tbv

    def test_to_dict(self):
        return self.build_sample().to_dict()

    def test_build_sample_and_compare_reference_file(self):
        """
        Build the sample, load the version from the reference file and
        compare them (in memory as opposed to "diffing" files)
        """
        json_tbv = self.build_sample().to_dict()
        json_tbv_reference = HelperTest().load_json_reference_file(
            'temporal_extension_bounding_volume_sample.json')
        if not json_tbv.items() == json_tbv_reference.items():
            self.fail()


if __name__ == "__main__":
    unittest.main()
