# -*- coding: utf-8 -*-

import json
import unittest
from temporal_extension import TemporalTileSet
from test_temporal_extension_primary_transaction \
    import Test_TemporalPrimaryTransaction
from test_temporal_extension_version import Test_TemporalVersion
from test_temporal_extension_version_transition \
    import Test_TemporalVersionTransition
from test_temporal_extension_transaction_aggregate \
    import Test_TemporalTransactionAggregate
from helper_test import HelperTest


class Test_TemporalTileSet(unittest.TestCase):
    """
    Batch Table extension of the Temporal applicative extension
    """

    @classmethod
    def build_sample(cls):
        """
        Programmatically define the reference a sample.
        :return: the sample as TemporalBatchTable object.
        """
        tts = TemporalTileSet()

        tts.set_start_date("2018-01-01")
        tts.set_end_date("2019-01-01")
        tts.set_transactions([Test_TemporalPrimaryTransaction.build_sample()])
        tts.set_versions([Test_TemporalVersion.build_sample()])
        tts.set_version_transitions([Test_TemporalVersionTransition.build_sample()])

        return tts

    def test_to_dict(self):
        return self.build_sample().to_dict()

    def test_build_sample_and_compare_reference_file(self):
        """
        Build the sample, load the version from the reference file and
        compare them (in memory as opposed to "diffing" files)
        """
        json_tbt = self.build_sample().to_dict()
        json_tbt_reference = HelperTest().load_json_reference_file(
            'temporal_extension_tileset_sample.json')
        # We do not want to compare the possible transaction identifiers:
        Test_TemporalTransactionAggregate.prune_id_from_nested_json_dict(
            json_tbt)
        Test_TemporalTransactionAggregate.prune_id_from_nested_json_dict(
            json_tbt_reference)
        if not json_tbt.items() == json_tbt_reference.items():
            self.fail()


if __name__ == "__main__":
    unittest.main()
